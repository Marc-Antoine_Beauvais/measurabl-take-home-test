package com.msr;

import com.msr.data.SiteRepository;
import com.msr.dto.SiteDto;
import com.msr.dto.UseTypesDto;
import com.msr.model.Site;
import com.msr.model.SiteUses;
import com.msr.model.UseTypes;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigInteger;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Intended as a starting point for Unit Testing of the BuildingsController class.
 */
@RunWith(SpringRunner.class)
public class BuildingsControllerTest {
    @Mock
    private SiteRepository siteRepositoryMock;

    private BuildingsController buildingsController;

    @Before
    public void setUp() {
        buildingsController = new BuildingsController(siteRepositoryMock);
    }

    @Test
    public void testSampleResponse_NullMessageParameter() throws Exception {
        String response = buildingsController.getSampleResponse(null, false);
        assertEquals(BuildingsController.NO_SAMPLE_PARAM_PROVIDED, response);
    }

    @Test
    public void testSampleResponse_EmptyMessageParameter() throws Exception {
        String response = buildingsController.getSampleResponse("", false);
        assertEquals(BuildingsController.NO_SAMPLE_PARAM_PROVIDED, response);
    }

    @Test
    public void testSampleResponse_MessageParameterProvided() throws Exception {
        String expectedString = "This is the expected output parameter.";
        String response = buildingsController.getSampleResponse(expectedString, false);
        assertEquals(BuildingsController.SAMPLE_PARAM_PROVIDED + expectedString, response);
    }

    @Test(expected = Exception.class)
    public void testSampleResponse_ErrorThrown_WithoutMessageParameter() throws Exception {
        buildingsController.getSampleResponse(null, true);
    }

    @Test(expected = Exception.class)
    public void testSampleResponse_ErrorThrown_WithMessageParameter() throws Exception {
        buildingsController.getSampleResponse("test", true);
    }

    /**
     * Intended to test the controller's get site by ID functionality.
     */
    @Test
    public void testGetSiteById() throws Exception {
        Site site = this.buildExpectedSite1();
        when(this.siteRepositoryMock.findById(1)).thenReturn(Optional.of(site));

        SiteDto siteDto = this.buildingsController.getById(1);
        assertEquals(
                siteDto,
                SiteDto.builder()
                        .id(1)
                        .name("Name site 1")
                        .address("707 Broadway Suite 1000")
                        .city("San Diego")
                        .state("CA")
                        .zipcode("92101")
                        .totalSize(BigInteger.valueOf(12000))
                        .primaryType(
                                UseTypesDto.builder()
                                        .id(1)
                                        .name("Name use type 1")
                                        .build()
                        )
                        .build()
        );
    }

    @Test(expected = Exception.class)
    public void testGetSiteById_ErrorThrown() throws Exception {
        this.buildingsController.getById(1);
    }

    /**
     * Intended to test the controller's get all sites functionality.
     */
    @Test
    public void testGetAllSite() {
        Site site1 = this.buildExpectedSite1();
        Site site2 = this.buildExpectedSite2();
        List<Site> sites = new ArrayList<>();
        sites.add(site1);
        sites.add(site2);

        when(this.siteRepositoryMock.findAll()).thenReturn(sites);

        Set<SiteDto> siteDto = this.buildingsController.getAll();

        Set<SiteDto> sitesDtoExpected = new HashSet<>();
        sitesDtoExpected.add(
                SiteDto.builder()
                        .id(1)
                        .name("Name site 1")
                        .address("707 Broadway Suite 1000")
                        .city("San Diego")
                        .state("CA")
                        .zipcode("92101")
                        .totalSize(null)
                        .primaryType(null)
                        .build()
        );
        sitesDtoExpected.add(
                SiteDto.builder()
                        .id(2)
                        .name("Name site 2")
                        .address("709 Broadway Suite 1000")
                        .city("Los Angeles")
                        .state("CA")
                        .zipcode("92101")
                        .totalSize(null)
                        .primaryType(null)
                        .build()
        );

        assertEquals(
                siteDto,
                sitesDtoExpected
        );
    }

    @Test
    public void testGetAllSite_Empty() {
        when(this.siteRepositoryMock.findAll()).thenReturn(new ArrayList<>());

        Set<SiteDto> siteDto = this.buildingsController.getAll();

        assertTrue(siteDto.isEmpty());
    }

    /**
     * Intended to test the controller's get sites by criteria functionality.
     */
    @Test
    public void testGetSiteByState() {
        Site site1 = this.buildExpectedSite1();
        Site site2 = this.buildExpectedSite2();
        List<Site> sites = new ArrayList<>();
        sites.add(site1);
        sites.add(site2);

        when(this.siteRepositoryMock.findByState("CA")).thenReturn(sites);

        Set<SiteDto> siteDto = this.buildingsController.getByCriteria("CA", Optional.empty());

        Set<SiteDto> sitesDtoExpected = new HashSet<>();
        sitesDtoExpected.add(
                SiteDto.builder()
                        .id(1)
                        .name("Name site 1")
                        .address("707 Broadway Suite 1000")
                        .city("San Diego")
                        .state("CA")
                        .zipcode("92101")
                        .totalSize(null)
                        .primaryType(null)
                        .build()
        );
        sitesDtoExpected.add(
                SiteDto.builder()
                        .id(2)
                        .name("Name site 2")
                        .address("709 Broadway Suite 1000")
                        .city("Los Angeles")
                        .state("CA")
                        .zipcode("92101")
                        .totalSize(null)
                        .primaryType(null)
                        .build()
        );

        assertEquals(
                siteDto,
                sitesDtoExpected
        );
    }

    @Test
    public void testGetSiteByStateAndCity() {
        Site site1 = this.buildExpectedSite1();
        List<Site> sites = new ArrayList<>();
        sites.add(site1);

        when(this.siteRepositoryMock.findByStateAndCity("CA", "San Diego")).thenReturn(sites);

        Set<SiteDto> siteDto = this.buildingsController.getByCriteria("CA", Optional.of("San Diego"));

        Set<SiteDto> sitesDtoExpected = new HashSet<>();
        sitesDtoExpected.add(
                SiteDto.builder()
                        .id(1)
                        .name("Name site 1")
                        .address("707 Broadway Suite 1000")
                        .city("San Diego")
                        .state("CA")
                        .zipcode("92101")
                        .totalSize(null)
                        .primaryType(null)
                        .build()
        );

        assertEquals(
                siteDto,
                sitesDtoExpected
        );
    }

    private Site buildExpectedSite1() {
        UseTypes useTypes1 = UseTypes.builder()
                .id(1)
                .name("Name use type 1")
                .build();
        UseTypes useTypes2 = UseTypes.builder()
                .id(2)
                .name("Name use type 2")
                .build();

        Set<SiteUses> siteUses = new HashSet<>();
        siteUses.add(
            SiteUses.builder()
                    .id(1)
                    .description("Description site use 1")
                    .sizeSqft(3000)
                    .useTypes(useTypes1)
                    .build()
        );
        siteUses.add(
            SiteUses.builder()
                    .id(2)
                    .description("Description site use 2")
                    .sizeSqft(4000)
                    .useTypes(useTypes1)
                    .build()
        );
        siteUses.add(
            SiteUses.builder()
                    .id(3)
                    .description("Description site use 3")
                    .sizeSqft(5000)
                    .useTypes(useTypes2)
                    .build()
        );

        return Site.builder()
                .id(1)
                .name("Name site 1")
                .address("707 Broadway Suite 1000")
                .city("San Diego")
                .state("CA")
                .zipcode("92101")
                .siteUses(siteUses)
                .build();
    }

    private Site buildExpectedSite2() {
        return Site.builder()
                .id(2)
                .name("Name site 2")
                .address("709 Broadway Suite 1000")
                .city("Los Angeles")
                .state("CA")
                .zipcode("92101")
                .build();
    }
}