package com.msr;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.msr.data.SiteRepository;
import com.msr.data.UseTypesRepository;
import com.msr.dto.SiteDto;
import com.msr.dto.SiteUsesDto;
import com.msr.dto.UseTypesDto;
import com.msr.util.ClasspathUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Respond to root requests
 *
 * @author Measurabl
 * @since 2019-05-23
 */
@Log4j2
@RestController
@RequestMapping("/")
public class RootController {
    private static final String DATA_SITES_JSON = "data/sites.json";
    private static final String DATA_USE_TYPES_JSON = "data/use_types.json";
    private static final String DATA_SITE_USES_JSON = "data/site_uses.json";

    private BuildProperties buildProperties;

    private final SiteRepository siteRepository;
    private final UseTypesRepository useTypesRepository;

    private ObjectMapper objectMapper;

    @Autowired
    public RootController(BuildProperties buildProperties,
                          SiteRepository siteRepository,
                          UseTypesRepository useTypesRepository,
                          ObjectMapper objectMapper) {
        this.buildProperties = buildProperties;
        this.siteRepository = siteRepository;
        this.useTypesRepository = useTypesRepository;
        this.objectMapper = objectMapper;
    }

    @GetMapping("/")
    public Map<String, Object> getRoot() {
        return ImmutableMap.of(
                "status", "up",
                "version", buildProperties.getVersion(),
                "name", buildProperties.getName(),
                "time", buildProperties.getTime()

        );
    }

    @Transactional
    public void initData() throws IOException {
        this.insertSites();
        this.insertUseTypes();
        this.insertSiteUses();
    }

    private void insertSites() throws IOException {
        String jsonSiteArray = ClasspathUtil.readFileToString(DATA_SITES_JSON, BuildingsApiApplication.class);
        Set<SiteDto> siteDtos = this.objectMapper.readValue(jsonSiteArray, new TypeReference<Set<SiteDto>>(){});
        this.siteRepository.saveAll(siteDtos.stream().map(SiteDto::convertToEntity).collect(Collectors.toSet()));
        log.info("Sites inserted from {}", DATA_SITES_JSON);
    }

    private void insertUseTypes() throws IOException {
        String jsonUseTypeArray = ClasspathUtil.readFileToString(DATA_USE_TYPES_JSON, BuildingsApiApplication.class);
        Set<UseTypesDto> useTypeDtos = this.objectMapper.readValue(jsonUseTypeArray, new TypeReference<Set<UseTypesDto>>(){});
        this.useTypesRepository.saveAll(useTypeDtos.stream().map(UseTypesDto::convertToEntity).collect(Collectors.toSet()));
        log.info("UseTypes inserted from {}", DATA_USE_TYPES_JSON);
    }

    void insertSiteUses() throws IOException {
        String jsonSiteUsesArray = ClasspathUtil.readFileToString(DATA_SITE_USES_JSON, BuildingsApiApplication.class);
        Set<SiteUsesDto> siteUsesDtos = this.objectMapper.readValue(jsonSiteUsesArray, new TypeReference<Set<SiteUsesDto>>(){});
        siteUsesDtos.forEach(siteUsesDto ->
            this.siteRepository.findById(siteUsesDto.getSiteId())
                .ifPresent(site ->
                    this.useTypesRepository.findById(siteUsesDto.getUseTypeId())
                        .ifPresent(useTypes -> {
                            site.getSiteUses().add(siteUsesDto.convertToEntity(useTypes));
                            this.siteRepository.save(site);
                        }))
        );
        log.info("SiteUses inserted from {}", DATA_SITE_USES_JSON);
    }
}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    