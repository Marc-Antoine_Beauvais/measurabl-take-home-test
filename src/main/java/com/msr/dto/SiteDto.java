package com.msr.dto;

import com.msr.model.Site;
import lombok.Builder;
import lombok.Data;

import java.math.BigInteger;

/**
 * Site info dto
 *
 * @author Marc-Antoine Beauvais
 * @since 2020-05-18
 */
@Data
@Builder
public class SiteDto {
    private int id;

    private String name;

    private String address;

    private String city;

    private String state;

    private String zipcode;

    private BigInteger totalSize;

    private UseTypesDto primaryType;

    public Site convertToEntity() {
        Site entity = new Site();
        entity.setId(this.id);
        entity.setName(this.name);
        entity.setAddress(this.address);
        entity.setCity(this.city);
        entity.setState(this.state);
        entity.setZipcode(this.zipcode);

        return entity;
    }
}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    