package com.msr.dto;

import com.msr.model.UseTypes;
import lombok.Builder;
import lombok.Data;

/**
 * Use types dto
 *
 * @author Marc-Antoine Beauvais
 * @since 2020-05-18
 */
@Data
@Builder
public class UseTypesDto {
    private int id;

    private String name;

    public UseTypes convertToEntity() {
        UseTypes entity = new UseTypes();
        entity.setId(this.id);
        entity.setName(this.name);

        return entity;
    }
}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    