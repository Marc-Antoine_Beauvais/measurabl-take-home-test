package com.msr.dto;

import com.msr.model.SiteUses;
import com.msr.model.UseTypes;
import lombok.Data;

/**
 * Site uses dto
 *
 * @author Marc-Antoine Beauvais
 * @since 2020-05-18
 */
@Data
public class SiteUsesDto {
    private int id;

    private String description;

    private long sizeSqft;

    private int useTypeId;

    private int siteId;

    public SiteUses convertToEntity(UseTypes useTypes) {
        SiteUses entity = new SiteUses();
        entity.setId(this.id);
        entity.setDescription(this.description);
        entity.setSizeSqft(this.sizeSqft);
        entity.setUseTypes(useTypes);

        return entity;
    }
}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    