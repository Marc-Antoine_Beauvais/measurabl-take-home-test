package com.msr.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Site uses
 *
 * @author Measurabl
 * @since 2019-06-11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class SiteUses {
    @Id
    private int id;

    private String description;

    private long sizeSqft;

    @ManyToOne
    private UseTypes useTypes;
}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    