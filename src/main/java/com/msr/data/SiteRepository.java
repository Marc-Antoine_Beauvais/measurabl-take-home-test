package com.msr.data;

import com.msr.model.Site;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Repository functionality for Site
 *
 * @author Measurabl
 * @since 2019-06-06
 */
public interface SiteRepository extends JpaRepository<Site, Integer> {
    List<Site> findByStateAndCity(String state, String city);

    List<Site> findByState(String state);
}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    