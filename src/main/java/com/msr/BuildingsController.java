package com.msr;

import com.msr.data.SiteRepository;
import com.msr.dto.SiteDto;
import com.msr.dto.UseTypesDto;
import com.msr.model.Site;
import com.msr.model.SiteUses;
import com.msr.model.UseTypes;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

/**
 * Respond to site requests
 *
 * @author Measurabl
 * @since 2019-06-06
 */
@RestController
@RequestMapping("/buildings")
public class BuildingsController {

    private final SiteRepository siteRepository;

    /* Sample Output messages. */
    private final static String SAMPLE_RESPONSE_BASE = "This is a sample response to test if your BuildingController is responding appropriately.  ";
    private final static String NO_SITE_EXCEPTION_MESSAGE = "No site for given parameters.";
    static final String SAMPLE_PARAM_PROVIDED = SAMPLE_RESPONSE_BASE + "The request param you passed was: ";
    static final String NO_SAMPLE_PARAM_PROVIDED = SAMPLE_RESPONSE_BASE + "No request param was provided.";
    static final String SAMPLE_EXCEPTION_MESSAGE = SAMPLE_RESPONSE_BASE + "An expected error was thrown.";

    public BuildingsController(SiteRepository siteRepository) {
        this.siteRepository = siteRepository;
    }

    /**
     * Used simply to check if your BuildingsController is responding to requests.
     * Has no function other than echoing.
     * @return A sample message based on the input parameters.
     * @throws Exception Only when 'throwError' is true.
     */
    @ApiOperation("Returns a sample message for baseline controller testing.")
    @GetMapping("/sample")
    public String getSampleResponse(@ApiParam("The message that will be echoed back to the user.")
                                    @RequestParam(required = false) final String message,
                                    @ApiParam("Forces this endpoint to throw a generic error.")
                                    @RequestParam(required = false) final boolean throwError) throws Exception {
        String response;
        if(throwError) {
            throw new Exception(SAMPLE_EXCEPTION_MESSAGE);
        } else if (StringUtils.isEmpty(message)){
            response = NO_SAMPLE_PARAM_PROVIDED;
        } else {
            response = SAMPLE_PARAM_PROVIDED + message;
        }
        return response;
    }

    /**
     * Used to return a site by id with its use-details (total size and primary type).
     *
     * @return A site based on the id with its use-details (total size and primary type).
     * @throws Exception Only when no site for given id.
     */
    @ApiOperation("Returns a site by id with its use-details (total size and primary type).")
    @GetMapping("/{id}")
    public SiteDto getById(@ApiParam("The id of the requested site.")
                               @PathVariable(value="id") final int id) throws Exception {
        Site site = this.siteRepository.findById(id).orElseThrow(() -> new Exception(NO_SITE_EXCEPTION_MESSAGE));
        UseTypes primaryType = this.getPrimaryType(site);

        SiteDto siteDto = this.getSiteDtoBuilder(site)
            .totalSize(this.getTotalSizeFromSite(site))
            .primaryType(
                UseTypesDto.builder()
                    .id(primaryType.getId())
                    .name(primaryType.getName())
                    .build()
            )
            .build();

        return siteDto;
    }

    /**
     * Used to return a list of all sites.
     *
     * @return All sites.
     */
    @ApiOperation("Returns a list of all sites.")
    @GetMapping("/")
    public Set<SiteDto> getAll() {
        Set<SiteDto> sites = this.siteRepository.findAll().stream()
                .map(site -> this.getSiteDtoBuilder(site).build())
                .collect(Collectors.toSet());

        return sites;
    }

    /**
     * Used to return all sites by state and city if necessary.
     *
     * @return All sites in given state and in city if given.
     */
    @ApiOperation("Returns a list of sites by state and city if given.")
    @GetMapping
    public Set<SiteDto> getByCriteria(@ApiParam("The state of the site you are looking for.")
                                          @RequestParam final String state,
                                      @ApiParam("The city of the site you are looking for.")
                                          @RequestParam final Optional<String> city) {
        Set<SiteDto> sites = city.map(cityName -> this.siteRepository.findByStateAndCity(state, cityName))
                .orElse(this.siteRepository.findByState(state))
                .stream()
                .map(site -> this.getSiteDtoBuilder(site).build())
                .collect(Collectors.toSet());

        return sites;
    }

    /**
     * Used to get the primary type, the largest useType by sizeSqft in aggregate per-site.
     *
     * @return The primary type.
     */
    private UseTypes getPrimaryType(Site site) {
        // Getting size by type in a map
        Map<UseTypes, BigInteger> sizeSqftByUseTypes = site.getSiteUses().stream()
            .collect(
                groupingBy(
                    SiteUses::getUseTypes,
                    Collectors.mapping(
                        siteUses -> BigInteger.valueOf(siteUses.getSizeSqft()),
                        Collectors.reducing(BigInteger.valueOf(0), BigInteger::add)
                    )
                )
            );

        // Get the one having the biggest size
        UseTypes primaryType = sizeSqftByUseTypes.entrySet().stream()
                                .max(Map.Entry.comparingByValue())
                                .map(Map.Entry::getKey)
                                .orElse(null);

        return primaryType;
    }

    /**
     * Used to get the total size of a site by summing the sizeSqft associated with.
     *
     * @return The total size of the site.
     */
    private BigInteger getTotalSizeFromSite(Site site) {
        BigInteger totalSize = site.getSiteUses().stream()
                .mapToLong(SiteUses::getSizeSqft)
                .mapToObj(BigInteger::valueOf)
                .reduce(BigInteger::add)
                .orElse(BigInteger.valueOf(0));

        return totalSize;
    }

    private SiteDto.SiteDtoBuilder getSiteDtoBuilder(Site site) {
        return SiteDto.builder()
                .id(site.getId())
                .name(site.getName())
                .address(site.getAddress())
                .city(site.getCity())
                .state(site.getState())
                .zipcode(site.getZipcode());
    }
}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    